import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";
// https://vitejs.dev/config/
export default defineConfig({
  alias: {
    /*
      路径别名
      若为文件系统路径必须是绝对路径的形式，否则将以别名原样呈现，不会解析为文件系统路径路径 
    */
    "@": path.resolve(__dirname, "src"),
  },
  base: "/", // 开发或生产环境服务的公共基础路径
  plugins: [vue()],
  //生产模式打包配置
  build: {
    outDir: "dist", //Specify the output directory (relative to project root).
  },
  define: {
    "process.env.NODE_ENV": `'${process.env.NODE_ENV}'`,
    __DEV__: process.env.NODE_ENV !== "production",
  },
  server: {
    host: "localhost", // 主机名
    port: 8080, // 端口
    open: true, // 自动打开浏览器
    // 配置代理规则
    proxy: {
      "/api": {
        target: "http://www.baidu.com",
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, ""),
      },
    },
    // cors: false, // 允许跨域，默认启用
    https: false,
  },
});
